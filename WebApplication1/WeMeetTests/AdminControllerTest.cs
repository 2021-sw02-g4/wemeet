﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Controllers;
using WebApplication1.Data;
using Xunit;

namespace WeMeetTests
{
    public class AdminControllerTest : IClassFixture<ContextFixture>
    {
        private WebApplication1Context _context;
        

        public AdminControllerTest(ContextFixture contextFixture)
        {
            _context = contextFixture.DbContext;

        }
        [Fact]
        public async Task isAdminTeste()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<User>>().Object,
                new IUserValidator<User>[0],
                new IPasswordValidator<User>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<User>>>().Object);


            var adminController = new AdministradorController(mockUserManager.Object, _context);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "testeAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isAdmin = true
            };

            _context.Users.Add(user);

            var result = await adminController.isAdmin(user, "testeAdmin@gmail.com");

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public async Task GerirAdminTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<User>>().Object,
                new IUserValidator<User>[0],
                new IPasswordValidator<User>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<User>>>().Object);


            var adminController = new AdministradorController(mockUserManager.Object, _context);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "testeAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            _context.Users.Add(user);

            var result = await adminController.GerirAdmin(user, "testeAdmin@gmail.com");

            Assert.IsType<RedirectToActionResult>(result);
        }

    }
}
