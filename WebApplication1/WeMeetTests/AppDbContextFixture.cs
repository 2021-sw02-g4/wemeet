﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WeMeetTests
{
    public class AppDbContextFixture : IDisposable
    {
        public WebApplication1Context DbContext { get; private set; }
        public UserManager<User> _userManager;
        
        public AppDbContextFixture()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<WebApplication1Context>().UseSqlServer(connection).Options;
            DbContext = new WebApplication1Context(options);
            DbContext.Database.EnsureCreated();
            var userStore = new UserStore<User>(DbContext);
            //_userManager = new ApplicationUserManager(userStore);
            var username = "Santiago";
            var myUser = new User()
            {
                UserName = username,
                Email = username + "@" + username + "." + username,
                EmailConfirmed = true,
                LockoutEnabled = false
            };
            var password = username;
            ///var result = userManager.CreateAsync(myUser, password);
            var myEvento = new EventoModel
            {
                Id = 1,
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = myUser,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoId = 1,
                EventoModel = myEvento,
                IsHost = true,
                User = myUser,
                UserId = myUser.Id

            };
            DbContext.UserEventos.Add(userEvento);
            DbContext.eventos.Add(myEvento);
            DbContext.SaveChanges();
        }
        public void Dispose() => DbContext.Dispose();
    }
}
