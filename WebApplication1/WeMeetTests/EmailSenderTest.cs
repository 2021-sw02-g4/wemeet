﻿using SendGrid;
using System.Threading.Tasks;
using WebApplication1.Services;
using Xunit;

namespace WeMeetTests
{
    public class EmailSenderTest
    {
        [Fact]
        public void SendEmailAsync_SendsEmail()
        {
            // Arranges
            var emailSender = new EmailSender();

            // Act
            var result = emailSender.SendEmailAsync("goncaloalcandeias@gmail.com", "Reset Password", "");

            // Assert      
            Assert.IsNotType<Task<Response>>(result);    
        }
    }
}
