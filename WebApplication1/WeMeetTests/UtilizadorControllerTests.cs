﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Controllers;
using WebApplication1.Data;
using WebApplication1.Models;
using Xunit;
using System.Web.Mvc;
using Microsoft.AspNetCore.Mvc;

namespace WeMeetTests
{
    public class UtilizadorControllerTests : IClassFixture<ContextFixture>
    {
        private WebApplication1Context _context;

        public UtilizadorControllerTests(ContextFixture contextFixture)
        {
            _context = contextFixture.DbContext;
        }
        [Fact]
        public async Task VerUtilizadoresTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "testeAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            _context.Users.Add(user);
            _context.SaveChangesAsync();

            var result = utilizadorController.VerUtilizadores();

            Assert.IsType<Microsoft.AspNetCore.Mvc.ViewResult>(result);

        }
        [Fact]
        public async Task avaliacoesAppUtilizadorTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var listAvaliacoes = new List<AvaliarApp>();

            var comment = "Boa app";

            var avaliacao = new AvaliarApp
            {
                User = user,
                UserId = user.Id,
                Comentario = comment,
                Rate = 4
            };

            listAvaliacoes.Add(avaliacao);
            var result = await utilizadorController.AvaliacoesAppUtilizador();
            Assert.IsType<Microsoft.AspNetCore.Mvc.ViewResult>(result);
        }
        [Fact]
        public async Task avaliarAppTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var comment = "Boa app";
            var rate = 4;

            _context.Users.Add(user);
            _context.SaveChangesAsync();

            var result = await utilizadorController.avaliarApp(user.Id, comment, rate);
            Assert.IsType<RedirectToActionResult>(result);

        }
        [Fact]
        public async Task DeleteAvaliacaoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "appAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var comment = "Boa app";

            var avaliacao = new AvaliarApp
            {
                User = user,
                UserId = user.Id,
                Comentario = comment,
                Rate = 4
            };

            _context.Users.Add(user);
            _context.AvaliarApp.Add(avaliacao);
            _context.SaveChangesAsync();

            var result = await utilizadorController.DeleteAvaliacao(avaliacao.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task PaginaPerfilUtilizadorTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);

            var user = new User
            {
                Name = "Anibal",
                Email = "anibal@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            var result = utilizadorController.PaginaPerfilUtilizador(user.Id);

            Assert.IsType<Microsoft.AspNetCore.Mvc.ViewResult>(result);
        }
        [Fact]
        public async Task FollowUserTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);

            var follower = new User
            {
                Name = "follower",
                Email = "follower@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var followed = new User
            {
                Name = "followed",
                Email = "followed@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            followed.Followers.Add(follower);

            var social = new SocialModel
            {
                UserIdFollowed = followed.Id,
                UserIdThatFollows = follower.Id
            };

            _context.Users.Add(followed);
            _context.Users.Add(follower);
            _context.Followers.Add(social);
            await _context.SaveChangesAsync();

            var result = await utilizadorController.FollowUser(followed.Id, follower.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task UnfollowUserTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);

            var follower = new User
            {
                Name = "follower",
                Email = "follower@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var followed = new User
            {
                Name = "followed",
                Email = "followed@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            followed.Followers.Add(follower);

            var social = new SocialModel
            {
                UserIdFollowed = followed.Id,
                UserIdThatFollows = follower.Id
            };

            _context.Users.Add(followed);
            _context.Users.Add(follower);
            _context.Followers.Add(social);
            await _context.SaveChangesAsync();

            var result = utilizadorController.UnfollowUser(followed.Id, follower.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task UserProfileDetailsTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);
            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);
            var follower = new User
            {
                Name = "follower",
                Email = "follower@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            _context.Users.Add(follower);
            await _context.SaveChangesAsync();
            var result = utilizadorController.UserProfileDetails(follower.Id);
            Assert.IsType<Microsoft.AspNetCore.Mvc.PartialViewResult>(result);
        }
        [Fact]
        public async Task searchUsersTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);
            var utilizadorController = new UtilizadorController(_context, mockUserManager.Object);
            var follower = new User
            {
                Name = "follower",
                Email = "follower@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            _context.Users.Add(follower);
            await _context.SaveChangesAsync();
            var result = utilizadorController.searchUsers("Nome");
            Assert.IsType<Microsoft.AspNetCore.Mvc.ViewResult>(result);
        }
    }
}
