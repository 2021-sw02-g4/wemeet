﻿//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.Services;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.Testing;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Text;
//using System.Threading.Tasks;
//using WebApplication1.Areas.Identity.Data;
//using WebApplication1.Controllers;
//using WebApplication1.Data;
//using WebApplication1.Infrastructure;
//using WebApplication1.Models;
//using Xunit;

//namespace WeMeetTests
//{
//    public class EventoControllerTest : IClassFixture<WebApplicationFactory<Program>>
//    {
//        private readonly WebApplicationFactory<Program> _factory;
//        private readonly HttpClient _client;
//        private readonly UserManager<User> _userManager;
//        private WebApplication1Context _context;
//        private readonly IUnitOfWork _unitOfWork;
//        private IEmailSender _emailSender;
//        public EventoControllerTest(WebApplicationFactory<Program> factory)
//        {
//            _factory = factory;
//            _client = _factory.CreateClient(
//                new WebApplicationFactoryClientOptions
//                {
//                    AllowAutoRedirect = false
//                });
//        }

//        [Fact]
//        public async Task CreateCommentRedirectToPage()
//        {
//            UserAthentication userLogin = new UserAthentication(_client);

//            userLogin.AuthenticateUser("zdvwhtbkspva@candassociates.com", "Teste123!").GetAwaiter();
//            // Act:
//            var httpResponse = await _client.GetAsync("Evento/ComentarioEvento");
//            // Assert: 
//            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
//        }
//        [Theory]
//        [InlineData(1, 2)]
//        public async Task ApagarCommentRedirectToPage(int? eventoId, int? comentarioId)
//        {
//            UserAthentication userLogin = new UserAthentication(_client);
//            userLogin.AuthenticateUser("zdvwhtbkspva@candassociates.com", "Teste123!").GetAwaiter();
//            var controller = new EventoController(_context, _userManager, _unitOfWork, _emailSender);
//            //var result = await controller.ApagarComentario(1, 1);
//            // Act:
//            var httpResponse = await _client.GetAsync("Evento/ComentarioEvento/" + comentarioId);
//            // Assert: 
//            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
//            //await Assert.IsType<Task<IActionResult>>(result);
//        }
//        [Theory]
//        [InlineData(1)]
//        public async Task FotoEventoRedirectToPage(int eventoId)
//        {
//            UserAthentication userLogin = new UserAthentication(_client);
//            userLogin.AuthenticateUser("zdvwhtbkspva@candassociates.com", "Teste123!").GetAwaiter();
//            var controller = new EventoController(_context, _userManager, _unitOfWork, _emailSender);
//            //var result =  controller.FotoEvento(eventoId);
//            // Act:
//            var httpResponse = await _client.GetAsync("Evento/FotoEvento/" + eventoId);
//            // Assert: 
//            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
//            //await Assert.IsType<Task<IActionResult>>(result);
//        }
//    }
//}
