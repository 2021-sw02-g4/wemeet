﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Controllers;
using WebApplication1.Data;
using WebApplication1.Models;
using Xunit;

namespace WeMeetTests
{
    public class SocialControllerTests : IClassFixture<ContextFixture>
    {
        private WebApplication1Context _context;

        public SocialControllerTests(ContextFixture contextFixture)
        {
            _context = contextFixture.DbContext;
        }

        //[Fact]
        //public async Task PaginaSocialTests()
        //{
        //    var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
        //        new Mock<IOptions<IdentityOptions>>().Object,
        //        new Mock<IPasswordHasher<User>>().Object,
        //        new IUserValidator<User>[0],
        //        new IPasswordValidator<User>[0],
        //        new Mock<ILookupNormalizer>().Object,
        //        new Mock<IdentityErrorDescriber>().Object,
        //        new Mock<IServiceProvider>().Object,
        //        new Mock<ILogger<UserManager<User>>>().Object);

        //    var paginaSocialController = new SocialController(_context, mockUserManager.Object);

        //    var user = new User
        //    {
        //        Name = "TesteAdmin",
        //        Email = "testeAdmin@gmail.com",
        //        PhoneNumber = "999999999",
        //        Hobbies = "Futebol",
        //        Country = "Portugal",
        //        Address = "Avenida das drenas",
        //        isAdmin = true
        //    };

        //    var listFeeds = new List<FeedPost>();

        //    var text = "teste post";

        //    var post = new FeedPost
        //    {
        //        UserId = user.Id,
        //        User = user,
        //        Post = text
        //    };

        //    listFeeds.Add(post);
        //    var result = await paginaSocialController.PaginaSocial(user.Id);
        //    Assert.IsType<ViewResult>(result);
        //}

        [Fact]
        public async Task AddPostTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var paginaSocialController = new SocialController(_context, mockUserManager.Object);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "testeAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var text = "teste do teste";

            _context.Users.Add(user);
            _context.SaveChangesAsync();

            var testes = _context.Users.FirstOrDefault(e => e.Email == user.Email);


            var result = await paginaSocialController.AddPost(testes.Id, text);
            Assert.IsType<RedirectToActionResult>(result);

        }

        [Fact]
        public async Task DeletePostTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var paginaSocialController = new SocialController(_context, mockUserManager.Object);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "testeAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var text = "teste do teste";

            var post = new FeedPost
            {
                UserId = user.Id,
                User = user,
                Post = text,
            };

            _context.Users.Add(user);
            _context.FeedPosts.Add(post);
            _context.SaveChangesAsync();

            var result = await paginaSocialController.DeletePost(post.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public async Task FollowUserSocialFeedTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var paginaSocialController = new SocialController(_context, mockUserManager.Object);

            var follower = new User
            {
                Name = "follower",
                Email = "follower@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var followed = new User
            {
                Name = "followed",
                Email = "followed@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            followed.Followers.Add(follower);

            var social = new SocialModel
            {
                UserIdFollowed = followed.Id,
                UserIdThatFollows = follower.Id
            };

            _context.Users.Add(followed);
            _context.Users.Add(follower);
            _context.Followers.Add(social);
            await _context.SaveChangesAsync();

            var result = await paginaSocialController.FollowUserSocialFeed(followed.Id, follower.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public async Task UnfollowUserSocialFeedTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var paginaSocialController = new SocialController(_context, mockUserManager.Object);

            var follower = new User
            {
                Name = "follower",
                Email = "follower@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var followed = new User
            {
                Name = "followed",
                Email = "followed@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var social = new SocialModel
            {
                UserIdFollowed = followed.Id,
                UserIdThatFollows = follower.Id
            };

            _context.Users.Add(followed);
            _context.Users.Add(follower);
            _context.Followers.Add(social);
            await _context.SaveChangesAsync();

            var result = await paginaSocialController.UnfollowUserSocialFeed(followed.Id, follower.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }
    }
}
