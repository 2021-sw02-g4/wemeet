﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Areas.Identity.Data;
using Xunit;

namespace WeMeetTests
{
    public class UserProfileTest
    {
        User user = new User
        {
            PhoneNumber = "911111111",
            Email = "goncaloalcandeias@gmail.com",
            Name = "Gonçalo Candeias",
            BirthDate = DateTime.Now,
            Age = 21,
            ImagePath = "",
            Address = "Sem Morada",
            isPrivate = false,
            Country = "Jupiter",
            Hobbies = "Sleep"
        };

        [Fact]
        public void EditEmailTest()
        {
            // Arranges
            var newEmail = "teste@gmail.com";

            // Act
            user.Email = newEmail;
            var result = newEmail;

            // Assert
            Assert.Equal(result, user.Email);
        }

        [Fact]
        public void EditNameTest()
        {
            // Arranges
            var newName = "Candeias Gonçalo";

            // Act
            user.Name = newName;
            var result = newName;

            // Assert
            Assert.Equal(result, user.Name);
        }

        [Fact]
        public void EditPhoneNumberTest()
        {
            // Arranges
            var newNumber = "91241224";

            // Act
            user.PhoneNumber = newNumber;
            var result = newNumber;

            // Assert
            Assert.Equal(result, user.PhoneNumber);
        }

        [Fact]
        public void EditAddressTest()
        {
            // Arranges
            var newAddress = "Rua Dos Moinhos, 3º Esq";

            // Act
            user.Address = newAddress;
            var result = newAddress;

            // Assert
            Assert.Equal(result, user.Address);
        }

        [Fact]
        public void EditPrivateTest()
        {
            // Arranges
            var newPrivate = true;

            // Act
            user.isPrivate = newPrivate;
            var result = newPrivate;

            // Assert
            Assert.Equal(result, user.isPrivate);
        }

        [Fact]
        public void EditCountryTest()
        {
            // Arranges
            var newCountry = "Portugal";

            // Act
            user.Country = newCountry;
            var result = newCountry;

            // Assert
            Assert.Equal(result, user.Country);
        }

        [Fact]
        public void EditHobbieTest()
        {
            // Arranges
            var newHobbie = "Football";

            // Act
            user.Hobbies = newHobbie;
            var result = newHobbie;

            // Assert
            Assert.Equal(result, user.Hobbies);
        }
    }
}
