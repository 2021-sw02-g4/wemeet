﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Data;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Areas.Identity.Data;

namespace WeMeetTests
{
    public class ContextFixture
    {
        public WebApplication1Context DbContext { get; private set; }

        public ContextFixture()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<WebApplication1Context>()
                .UseSqlite(connection)
                .Options;
            DbContext = new WebApplication1Context(options);


            DbContext.Database.EnsureCreated();

            


        }

       
    }
}
