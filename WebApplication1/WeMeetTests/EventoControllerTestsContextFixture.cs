﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Controllers;
using WebApplication1.Data;
using WebApplication1.Infrastructure;
using WebApplication1.Models;
using WebApplication1.Services;
using Xunit;
//using System.Web.Mvc;

namespace WeMeetTests
{
    public class EventoControllerTestsContextFixture : IClassFixture<ContextFixture>
    {
        private WebApplication1Context _context;
        private IUnitOfWork _unitOfWork;
        private IEmailSender _emailSender;

        public EventoControllerTestsContextFixture(ContextFixture contextFixture)
        {
            _context = contextFixture.DbContext;
        }
        [Fact]
        public async Task HistConqEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<IPasswordHasher<User>>().Object,
                 new IUserValidator<User>[0],
                 new IPasswordValidator<User>[0],
                 new Mock<ILookupNormalizer>().Object,
                 new Mock<IdentityErrorDescriber>().Object,
                 new Mock<IServiceProvider>().Object,
                 new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var result = eventoController.HistConqEvento();

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task DeleteEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<User>>().Object,
                new IUserValidator<User>[0],
                new IPasswordValidator<User>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id

            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.DeleteEvento(myEvento.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task DeleteEventoInicialTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<User>>().Object,
                new IUserValidator<User>[0],
                new IPasswordValidator<User>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id

            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.DeleteEventoInicial(myEvento.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task DeleteEventoHistoricoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<User>>().Object,
                new IUserValidator<User>[0],
                new IPasswordValidator<User>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id

            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.DeleteEventoHistorico(myEvento.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task DisableEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<User>>().Object,
                new IUserValidator<User>[0],
                new IPasswordValidator<User>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id

            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.DisableEvento(myEvento.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task EntrarEvento()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
                new Mock<IOptions<IdentityOptions>>().Object,
                new Mock<IPasswordHasher<User>>().Object,
                new IUserValidator<User>[0],
                new IPasswordValidator<User>[0],
                new Mock<ILookupNormalizer>().Object,
                new Mock<IdentityErrorDescriber>().Object,
                new Mock<IServiceProvider>().Object,
                new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.EntrarEvento(myEvento.Id, user.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task SairEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id

            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();


            var result = await eventoController.SairEvento(myEvento.Id, user.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        //[Fact]
        //public async Task InicialEventosTest()
        //{
        //    var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
        //         new Mock<IOptions<IdentityOptions>>().Object,
        //         new Mock<IPasswordHasher<User>>().Object,
        //         new IUserValidator<User>[0],
        //         new IPasswordValidator<User>[0],
        //         new Mock<ILookupNormalizer>().Object,
        //         new Mock<IdentityErrorDescriber>().Object,
        //         new Mock<IServiceProvider>().Object,
        //         new Mock<ILogger<UserManager<User>>>().Object);

        //    var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

        //    var result = eventoController.InicialEventos();

        //    Assert.IsType<ViewResult>(result);
        //}
        [Fact]
        public async Task ComentarEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id

            };

            var text = "Olá";

            var comentario = new Comentario
            {
                Comment = text,
                EventoId = myEvento.Id,
                UserId = user.Id
            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.Comentarios.Add(comentario);
            _context.SaveChanges();

            var result = await eventoController.ComentarEvento(myEvento.Id, text, user.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task ComentarioEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id

            };

            var text = "Olá";

            var comentario = new Comentario
            {
                Comment = text,
                EventoId = myEvento.Id,
                UserId = user.Id
            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.Comentarios.Add(comentario);
            _context.SaveChanges();

            var result = await eventoController.ComentarioEvento(myEvento.Id);

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task FotoEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id

            };

            var foto = "";

            var myFoto = new FotoModel
            {
                User = user.Id,
                EventoId =  myEvento.Id,
                ImagePath = foto
            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.Fotos.Add(myFoto);
            _context.SaveChanges();

            var result = eventoController.FotoEvento(myEvento.Id);

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task VerEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var result = eventoController.VerEvento();

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task DetalheEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id
            };

            myEvento.UserEventos.Add(userEvento);

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.DetalheEvento(myEvento.Id);

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task RemoveUsersPostTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var userHost = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = userHost,
                UserId = userHost.Id
            };

            var userEvento2 = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id
            };

            myEvento.UserEventos.Add(userEvento);
            myEvento.UserEventos.Add(userEvento2);

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.RemoveUsers(user.Id, myEvento.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task RemoveUsersGetTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var userHost = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = userHost,
                UserId = userHost.Id
            };

            var userEvento2 = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id
            };

            myEvento.UserEventos.Add(userEvento);
            myEvento.UserEventos.Add(userEvento2);

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.RemoveUsers(myEvento.Id);

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task ApagarComentarioTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };


            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id
            };

            var text = "Olá";

            var comentario = new Comentario
            {
                Comment = text,
                EventoId = myEvento.Id,
                UserId = user.Id
            };

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.Comentarios.Add(comentario);
            _context.SaveChanges();

            var result = await eventoController.ApagarComentario(comentario.Id, myEvento.Id, user.Id);

            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task DetalheEventoDisableTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id
            };
            myEvento.UserEventos.Add(userEvento);
            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();
            var result = await eventoController.DetalheEventoDisable(myEvento.Id);
            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task AddUsersGetTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);

            var userHost = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };

            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };

            var userEvento = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = true,
                User = userHost,
                UserId = userHost.Id
            };

            var userEvento2 = new UserEvento
            {
                EventoModel = myEvento,
                IsHost = false,
                User = user,
                UserId = user.Id
            };

            myEvento.UserEventos.Add(userEvento);
            myEvento.UserEventos.Add(userEvento2);

            _context.UserEventos.Add(userEvento);
            _context.eventos.Add(myEvento);
            _context.SaveChanges();

            var result = await eventoController.AddUsers(myEvento.Id);

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task AddUsersPostTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var userHost = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            _context.eventos.Add(myEvento);
            _context.SaveChanges();
            var result = await eventoController.AddUsers(user.Id, myEvento.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task AdicionarFotoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);
            var content = "Hello World from a Fake File";
            var fileName = "test.pdf";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(content);
            writer.Flush();
            stream.Position = 0;
            //var file = fileMock.Object;
            IFormFile file = new FormFile(stream, 0, stream.Length, "id", fileName);
            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var foto = new FotoModel
            {
                EventoId = myEvento.Id,
                ImagePath = "",
                User = user.Id
            };
            _context.eventos.Add(myEvento);
            _context.Fotos.Add(foto);
            _context.SaveChanges();
            var result = await eventoController.AdicionarFoto(myEvento.Id, file,user.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task ApagarFotoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);

            var content = "Hello World from a Fake File";
            var fileName = "test.pdf";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(content);
            writer.Flush();
            stream.Position = 0;
            IFormFile file = new FormFile(stream, 0, stream.Length, "id", fileName);
            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var foto = new FotoModel
            {
                EventoId = myEvento.Id,
                ImagePath = "",
                User = user.Id
            };
            _context.eventos.Add(myEvento);
            _context.Fotos.Add(foto);
            _context.SaveChanges();
            var result = await eventoController.ApagarFoto(foto.Id, myEvento.Id);
            Assert.IsType <ViewResult>(result);
        }
        [Fact]
        public async Task ClassificarTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);
            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var classificacao = new Classificacao
            {
                EventoId = myEvento.Id,
                Rate = 5,
                UserId = user.Id
            };
            var userEvento = new UserEvento
            {
                EventoId = myEvento.Id,
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id
            };
            _context.eventos.Add(myEvento);
            _context.UserEventos.Add(userEvento);
            _context.Classificacoes.Add(classificacao);
            _context.SaveChanges();
            var result = await eventoController.Classificar(myEvento.Id, 5, user.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task CriarEventoTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);
            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoId = myEvento.Id,
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id
            };
            _context.Users.Add(user);
            _context.SaveChanges();
            var result = await eventoController.CriarEvento(myEvento, null, user.Id);
            Assert.IsType<RedirectToActionResult>(result);
        }
        [Fact]
        public async Task EditarEventoGetTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);
            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoId = myEvento.Id,
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id
            };
            _context.eventos.Add(myEvento);
            _context.UserEventos.Add(userEvento);
            _context.Users.Add(user);
            _context.SaveChanges();
            var result = await eventoController.EditarEvento(myEvento.Id);
            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public async Task EditarEventoPostTest()
        {
            var mockUserManager = new Mock<UserManager<User>>(new Mock<IUserStore<User>>().Object,
               new Mock<IOptions<IdentityOptions>>().Object,
               new Mock<IPasswordHasher<User>>().Object,
               new IUserValidator<User>[0],
               new IPasswordValidator<User>[0],
               new Mock<ILookupNormalizer>().Object,
               new Mock<IdentityErrorDescriber>().Object,
               new Mock<IServiceProvider>().Object,
               new Mock<ILogger<UserManager<User>>>().Object);
            var eventoController = new EventoController(_context, mockUserManager.Object, _unitOfWork, _emailSender);
            var user = new User
            {
                Name = "TesteAdmin",
                Email = "novoAdmin@gmail.com",
                PhoneNumber = "999999999",
                Hobbies = "Futebol",
                Country = "Portugal",
                Address = "Avenida das drenas",
                isActive = true
            };
            var myEvento = new EventoModel
            {
                Country = "Portugal",
                Description = "Jogo de Futebol",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Futebolada",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var myEvento2 = new EventoModel
            {
                Country = "Portugal",
                Description = "Hoquei",
                isActive = true,
                IsPrivate = false,
                IsRecurring = false,
                Location = "Lisboa",
                Sport = "Futebol",
                Name = "Hoquei",
                HostUser = user,
                Date = DateTime.Now,
                UserLimit = 5,
                UserEventos = new List<UserEvento>(),
                ImagePath = ""
            };
            var userEvento = new UserEvento
            {
                EventoId = myEvento.Id,
                EventoModel = myEvento,
                IsHost = true,
                User = user,
                UserId = user.Id
            };
            _context.eventos.Add(myEvento);
            _context.UserEventos.Add(userEvento);
            _context.Users.Add(user);
            _context.SaveChanges();
            var result = await eventoController.EditarEvento(myEvento.Id, myEvento2, null);
            Assert.IsType<RedirectToActionResult>(result);
        }
    }
}
