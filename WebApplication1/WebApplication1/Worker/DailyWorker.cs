﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Worker
{
    public class DailyWorker : BackgroundService
    {
        protected IServiceProvider serviceProvider;
        protected IEmailSender emailSender;

        public DailyWorker(IServiceProvider serviceProvider, IEmailSender emailSender)
        {
            this.serviceProvider = serviceProvider;
            this.emailSender = emailSender;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            System.Diagnostics.Debug.WriteLine("Teste");
            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = serviceProvider.CreateScope())
                {
                    var dbContext = (WebApplication1Context)scope.ServiceProvider.GetRequiredService(typeof(WebApplication1Context));

                    //chamar aqui as funcoes para enviar os emails
                    //TIRAR COMENTARIO PARA O DEPLOY SENAO SPAMMA SEMPRE QUE ABRIR
                    //eventoProximo(dbContext);
                }
                await Task.Delay(86400000);
            }
        }

        public async void eventoProximo(WebApplication1Context dbContext)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                dbContext = (WebApplication1Context)scope.ServiceProvider.GetRequiredService(typeof(WebApplication1Context));

                var eventos = await dbContext.UserEventos.Include(uId => uId.User).Include(eId => eId.EventoModel).Where(e => e.EventoModel.Date.Day == DateTime.Now.AddDays(1).Day && e.EventoModel.Date.Month == DateTime.Now.Month).ToListAsync();
                foreach (var user in eventos)
                {
                    await emailSender.SendEmailAsync(user.User.Email, "Evento Próximo", $"O evento <br> Nome Evento : {user.EventoModel.Name} <br> irá acontecer dentro de 1 dia");
                }
            }

        }
    }
}
