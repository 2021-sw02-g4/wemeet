﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class SocialController : Controller
    {

        private readonly WebApplication1Context _context;
        private readonly UserManager<User> _userManager;

        public SocialController(WebApplication1Context context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        /// <summary>
        /// Função para mostrar publicações da página inicial do user e de pessoas que ele segue
        /// </summary>
        /// <returns>Retorna a página inicial coma s publicações</returns>
        public async Task<IActionResult> PaginaSocial()
        {
            var curUser = _userManager.GetUserAsync(User).Result;
            //var curUser = _userManager.FindByIdAsync(id).Result;
            var posts = _context.FeedPosts.ToList();


            var following = _context.Followers.Where(f => curUser.Id.Equals(f.UserIdThatFollows)).ToList();

            var postsFeed = new HashSet<FeedPost>();
            var curPosts = new List<FeedPost>();

            foreach (var myPosts in posts)
            {
                if (myPosts.UserId.Equals(curUser.Id))
                {
                    curPosts.Add(myPosts);
                }
            }

            postsFeed.UnionWith(curPosts);

            foreach (var allposts in posts)
            {
               foreach(var followinPosts in following)
                {
                    if(allposts.UserId.Equals(followinPosts.UserIdFollowed))
                    {
                        postsFeed.Add(allposts);
                    }
                }
            }

            return View(postsFeed);
        }

        /// <summary>
        /// Função para adicionar um post há página
        /// </summary>
        /// <param name="id">Id do user</param>
        /// <param name="text">texto do post</param>
        /// <returns>Retorna página de publicações atualizada com o post novo</returns>
        [HttpPost]
        public async Task<IActionResult> AddPost(string id, string text)
        {
            //var user = _userManager.FindByIdAsync(id).Result;
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            var post = new FeedPost
            {
                UserId = id,
                User = user,
                Post = text,
            };

            _context.FeedPosts.AddAsync(post);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(PaginaSocial), new { id = user.Id });
        }

        /// <summary>
        /// Função para apagar um post(publicação)
        /// </summary>
        /// <param name="id">Id do post</param>
        /// <returns>Retorna Página de publicações atualizada sem o post</returns>
        public async Task<IActionResult> DeletePost(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var post = _context.FeedPosts.FirstOrDefault(e => e.Id == id);
                _context.FeedPosts.Remove(post);
                await _context.SaveChangesAsync();
                TempData["SuccessoPost"] = "Publicação apagada com sucesso";
                return RedirectToAction(nameof(PaginaSocial));
            }catch
            {
                TempData["ErroPost"] = "Não foi possivel apagar publicação";
                return RedirectToAction(nameof(PaginaSocial));
            }
               
        }

        /// <summary>
        /// Função para Seguir users que aparecam na página de publicações
        /// </summary>
        /// <param name="id">id do user</param>
        /// <returns>Retorna página de publicações com o user seguido no lugar das pessoas que segue</returns>
        [HttpPost]
        public async Task<IActionResult> FollowUserSocialFeed(string? followedId, string? curUserId)
        {
            var followers = _context.Followers.Where(u => u.UserIdFollowed == followedId).ToList();
            if (followedId == null)
            {
                return NotFound();
            }
            var user = _context.Users.FirstOrDefault(u => u.Id == followedId);
            if (user == null)
            {
                return NotFound();
            }
            var curUser = _context.Users.FirstOrDefault(u => u.Id == curUserId);
            foreach (var followed in followers)
            {
                if (followed.UserIdFollowed.Equals(user.Id))
                {
                    user.Followers.Add(_context.Users.FirstOrDefault(u => u.Id == followed.UserIdThatFollows));
                    //_userManager.FindByIdAsync(followed.UserIdThatFollows).Result);
                }
            }
            user.Followers.Add(curUser);
            var socialInsert = new SocialModel
            {
                UserIdFollowed = user.Id,
                UserIdThatFollows = curUser.Id
            };
            _context.Followers.Add(socialInsert);
            await _context.SaveChangesAsync();

            var counterFollowings = _context.Followers.Where(u => curUser.Id == u.UserIdThatFollows).ToList().Count();
            var counterFollowers = _context.Followers.Where(u => user.Id == u.UserIdFollowed).ToList().Count();
            var conqFollowingFlag = false;
            var conqFollowersFlag = false;
            if (counterFollowers == 5)
            {
                foreach (var conq in _context.ConquistasUser.ToList())
                {
                    if (conq.UserId.Equals(user.Id))
                    {
                        if (conq.ConquistId == 2)
                        {
                            conqFollowersFlag = true;
                        }
                    }
                }
                if (!conqFollowersFlag)
                {
                    var novaCOnquista = new ConquistaUser
                    {
                        UserId = user.Id,
                        ConquistId = 2,
                        DateAchieved = DateTime.Now,
                    };
                    _context.ConquistasUser.Add(novaCOnquista);
                }
            }
            await _context.SaveChangesAsync();
            if (counterFollowings == 5)
            {
                foreach (var conq in _context.ConquistasUser.ToList())
                {
                    if (conq.UserId.Equals(curUser.Id))
                    {
                        if (conq.ConquistId == 3)
                        {
                            conqFollowingFlag = true;
                        }
                    }
                }
                if (!conqFollowingFlag)
                {
                    var novaCOnquista = new ConquistaUser
                    {
                        UserId = curUser.Id,
                        ConquistId = 3,
                        DateAchieved = DateTime.Now,
                    };
                    _context.ConquistasUser.Add(novaCOnquista);
                }
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(PaginaSocial));
        }

        /// <summary>
        /// Função para deixar de seguir um user na página de publicações
        /// </summary>
        /// <param name="id">Id do user</param>
        /// <returns>Retorna página de publicações com o user seguido no lugar das pessoas que não segue</returns>
        [HttpPost]
        public async Task<IActionResult> UnfollowUserSocialFeed(string? followedId, string? curUserId)
        {
            var followers = _context.Followers.Where(u => u.UserIdFollowed == followedId).ToList();
            if (followedId == null)
            {
                return NotFound();
            }
            var user = _context.Users.FirstOrDefault(u => u.Id == followedId);
            if (user == null)
            {
                return NotFound();
            }
            foreach (var followed in followers)
            {
                if (followed.UserIdFollowed.Equals(user.Id))
                {
                    user.Followers.Add(_context.Users.FirstOrDefault(u => u.Id == followed.UserIdThatFollows));
                }
            }
            var curUser = _context.Users.FirstOrDefault(u => u.Id == curUserId);
            foreach (var f in followers)
            {
                if (f.UserIdThatFollows.Equals(curUser.Id) && f.UserIdFollowed.Equals(user.Id))
                {
                    user.Followers.Remove(curUser);
                    _context.Followers.Remove(f);
                }
            }
            _context.SaveChanges();
            return RedirectToAction(nameof(PaginaSocial));
        }
    }
}
