﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Web.Mvc.Ajax;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Data;
using WebApplication1.Models;
namespace WebApplication1.Controllers
{
    public class UtilizadorController : Controller
    {
        private readonly WebApplication1Context _context;
        private readonly UserManager<User> _userManager;
        public UtilizadorController(WebApplication1Context context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;            
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ConquistasUtilizador()
        {
            return View();
        }
        /// <summary>
        /// Função para retornar todos os users da aplicação
        /// </summary>
        /// <returns>Retorna uma página com todos os utilizadores</returns>
        public IActionResult VerUtilizadores()
        {
            var users = _context.Users.ToList();
            return View(users);
        }

        /// <summary>
        /// Função para mostrar o perfil de um utilizador
        /// </summary>
        /// <param name="id">Id do user</param>
        /// <returns>Retorna uma página de perfil de um utilizador escolhido</returns>
        [HttpGet]
        public IActionResult PaginaPerfilUtilizador(string? id)
        {
            var followers = _context.Followers.Where(u => u.UserIdFollowed == id).ToList();
            if (id == null)
            {
                return NotFound();
            }
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            foreach (var followed in followers)
            {
                if (followed.UserIdFollowed.Equals(user.Id))
                {
                    user.Followers.Add(_userManager.FindByIdAsync(followed.UserIdThatFollows).Result);
                }
            }
            return View(user);
        }

        /// <summary>
        /// Função para seguir outro User
        /// </summary>
        /// <param name="id">Id do User</param>
        /// <returns>Retorna página de perfil desse user atualizada sem botão para seguir</returns>
        [HttpPost]
        public async Task<IActionResult> FollowUser(string? id, string curUserId)
        {
            var followers = _context.Followers.Where(u => u.UserIdFollowed == id).ToList();
            if (id == null)
            {
                return NotFound();
            }
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            var curUser = _context.Users.FirstOrDefault(u => u.Id == curUserId);
            foreach (var followed in followers)
            {
                if (followed.UserIdFollowed.Equals(user.Id))
                {
                    user.Followers.Add(_context.Users.FirstOrDefault(u => u.Id == followed.UserIdThatFollows));
                }
            }
            user.Followers.Add(curUser);
            var socialInsert = new SocialModel
            {
                UserIdFollowed = user.Id,
                UserIdThatFollows = curUser.Id
            };
            _context.Followers.Add(socialInsert);
            _context.SaveChanges();

            var counterFollowings = _context.Followers.Where(u => curUser.Id == u.UserIdThatFollows).ToList().Count();
            var counterFollowers = _context.Followers.Where(u => user.Id == u.UserIdFollowed).ToList().Count();
            var conqFollowingFlag = false;
            var conqFollowersFlag = false;
            if (counterFollowers == 5)
            {
                foreach (var conq in _context.ConquistasUser.ToList())
                {
                    if (conq.UserId.Equals(user.Id))
                    {
                        if (conq.ConquistId == 2)
                        {
                            conqFollowersFlag = true;
                        }
                    }
                }
                if (!conqFollowersFlag)
                {
                    var novaCOnquista = new ConquistaUser
                    {
                        UserId = user.Id,
                        ConquistId = 2,
                        DateAchieved = DateTime.Now,
                    };
                    _context.ConquistasUser.Add(novaCOnquista);
                }
            }
            await _context.SaveChangesAsync();
            if (counterFollowings == 5)
            {
                foreach (var conq in _context.ConquistasUser.ToList())
                {
                    if (conq.UserId.Equals(curUser.Id))
                    {
                        if (conq.ConquistId == 3)
                        {
                            conqFollowingFlag = true;
                        }
                    }
                }
                if (!conqFollowingFlag)
                {
                    var novaCOnquista = new ConquistaUser
                    {
                        UserId = curUser.Id,
                        ConquistId = 3,
                        DateAchieved = DateTime.Now,
                    };
                    _context.ConquistasUser.Add(novaCOnquista);
                }
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(PaginaPerfilUtilizador), new { id = user.Id });
        }

        /// <summary>
        /// Função para deixar de seguir um User
        /// </summary>
        /// <param name="id">Id do user</param>
        /// <returns>Retorna página de perfil desse user atualizada sem botão para deixar de seguir</returns>
        [HttpPost]
        public IActionResult UnfollowUser(string? id, string curUserId)
        {
            var followers = _context.Followers.Where(u => u.UserIdFollowed == id).ToList();
            if (id == null)
            {
                return NotFound();
            }
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            foreach (var followed in followers)
            {
                if (followed.UserIdFollowed.Equals(user.Id))
                {
                    user.Followers.Add(_context.Users.FirstOrDefault(u => u.Id == followed.UserIdThatFollows));
                }
            }
            var curUser = _context.Users.FirstOrDefault(u => u.Id == curUserId);
            foreach (var f in followers)
            {
                if (f.UserIdThatFollows.Equals(curUser.Id) && f.UserIdFollowed.Equals(user.Id))
                {
                    user.Followers.Remove(curUser);
                    _context.Followers.Remove(f);
                }
            }
            _context.SaveChanges();
            return RedirectToAction(nameof(PaginaPerfilUtilizador), new { id = user.Id });
        }

        /// <summary>
        /// Função para ter os detalhes de um user
        /// </summary>
        /// <param name="userSearch">nome a ser procurado</param>
        /// <returns>Retorna os detalhes do user procurado</returns>
        [HttpGet]
        public IActionResult UserProfileDetails(string? userSearch)
        {
            foreach (var user in _context.Users.ToList())
            {
                if (user.Id.Equals(userSearch))
                {
                    return PartialView("_UtilizadorPartial", user);
                }
            }
            return NotFound();
        }

        /// <summary>
        /// Função para procurar um user
        /// </summary>
        /// <param name="searchString">Nome do user a procurar</param>
        /// <returns>Retorna o User procurado</returns>
        [HttpGet]
        public IActionResult searchUsers(string? searchString)
        {
            var usersList = new List<User>();
            foreach (var user in _context.Users.ToList())
            {
                if (user.Name.ToLower().Contains(searchString.ToLower()))
                {
                    usersList.Add(user);
                }
            }
            return View("VerUtilizadores", usersList);
        }

        /// <summary>
        /// Função para mostrar avaliações da app
        /// </summary>
        /// <returns>Retorna uma lista de avaliações</returns>
        public async Task<IActionResult> AvaliacoesAppUtilizador()
        {
            var avaliacoes = _context.AvaliarApp.ToList();
            foreach (var avaliacaouser in avaliacoes)
            {
                avaliacaouser.User = _context.Users.FirstOrDefault(a => a.Id == avaliacaouser.UserId);
            }
            return View(avaliacoes);
        }

        /// <summary>
        /// Função para avaliar a app
        /// </summary>
        /// <param name="id">Id do user</param>
        /// <param name="comment">Comentário da avaliação</param>
        /// <param name="rate">Rate da avaliação</param>
        /// <returns>Retorna página de avaliações com a nova avaliação</returns>
        [HttpPost]
        public async Task<IActionResult> avaliarApp(string id, string comment, int rate)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                var avaliacaoApp = new AvaliarApp
                {
                    UserId = id,
                    User = user,
                    Comentario = comment,
                    Rate = rate
                };
                _context.AvaliarApp.AddAsync(avaliacaoApp);
                await _context.SaveChangesAsync();

                TempData["SuccessoAvaliar"] = "Avaliação feita com sucesso!";
                return RedirectToAction(nameof(AvaliacoesAppUtilizador));
            }catch
            {
                TempData["ErroAvaliar"] = "Não foi possivel avaliar a app";
                return RedirectToAction(nameof(AvaliacoesAppUtilizador));
            }
                
        }
        public async Task<IActionResult> DeleteAvaliacao(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var avaliacao = _context.AvaliarApp.FirstOrDefault(e => e.Id == id);
                _context.AvaliarApp.Remove(avaliacao);
                await _context.SaveChangesAsync();
                TempData["SuccessoAvaliacaoApagada"] = "Avaliação apagada com sucesso";
                return RedirectToAction(nameof(AvaliacoesAppUtilizador));
            }
            catch
            {
                TempData["ErroAvaliacaoApagada"] = "Não foi possivel apagar a avaliação";
                return RedirectToAction(nameof(AvaliacoesAppUtilizador));
            }
                
        }
    }
}