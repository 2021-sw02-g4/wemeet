﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Data;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class AdministradorController : Controller
    {
        private readonly UserManager<User> userManager;
        private DbContextOptions<WebApplication1Context> options;
        private readonly WebApplication1Context _context;

        public AdministradorController( UserManager<User> userManager, WebApplication1Context context)
        {
            this.userManager = userManager;
            this._context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult InicioAdmin()
        {
            return View();
        }

        /// <summary>
        /// Função para retornar todos os utilizadores da aplicação
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GerirAdmin()
        {
            var users = userManager.Users;
            return View(users);

        }


        [HttpPost]
        ///<summary>
        ///Função que verifica o estado do admin e troca o para outro, caso venha a true, coloca a false
        ///</summary>
        ///<param name="userData">um user</param>
        ///<param name="admin">email do admin</param>
        ///<returns>retorna a view da pagina gerir admins</returns>
        public async Task<IActionResult> isAdmin(User userData, string admin)
        {
            try
            {
                var options = new DbContextOptions<WebApplication1Context>();
                var user = await userManager.FindByEmailAsync(admin);

                foreach (var p in _context.Users)
                {
                    if (p.Email.Equals(admin))
                    {
                        if (p.isAdmin == false)
                        {
                            p.isAdmin = true;
                        }
                        else
                        {
                            p.isAdmin = false;
                        }
                        _context.Users.Update(p);
                    }
                }
                await _context.SaveChangesAsync();
                TempData["SuccessoAdmin"] = "Utilizador colocado como admin";
                return RedirectToAction(nameof(GerirAdmin));
            }catch
            {
                TempData["ErroAdmin"] = "Não foi possivel concluir a ação";
                return RedirectToAction(nameof(GerirAdmin));
            }
                
        }

        /// <summary>
        /// Altera o estado da conta de ativado para desativado ou  vice versa
        /// </summary>
        /// <param name="userData">um user</param>
        /// <param name="teste">email do utilizador</param>
        /// <returns>retorna a view da pagina gerir admins</returns>
        public async Task<IActionResult> GerirAdmin(User userData, string teste)
        {
            try
            {
                var options = new DbContextOptions<WebApplication1Context>();
                var user = await userManager.FindByEmailAsync(teste);

                foreach (var p in _context.Users)
                {
                    if (p.Email.Equals(teste))
                    {
                        if (p.isActive == false)
                        {
                            p.isActive = true;
                        }
                        else
                        {
                            
                            p.isActive = false;
                        }
                        _context.Users.Update(p);
                    }
                }
                await _context.SaveChangesAsync();
                TempData["SuccessoSuspender"] = "Estado de conta alterado com sucesso";
                return RedirectToAction(nameof(GerirAdmin));
            }
            catch
            {
                TempData["ErroSuspender"] = "Não foi possivel realizar esta operação";
                return RedirectToAction(nameof(GerirAdmin));
            }
            
        }
    }
}
