﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Data;
using WebApplication1.Infrastructure;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class EventoController : Controller
    {
        private readonly WebApplication1Context _context;
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        protected IEmailSender _emailSender;
        public EventoController(WebApplication1Context context, UserManager<User> userManager, IUnitOfWork unitOfWork, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _emailSender = emailSender;
        }
        public async Task<IActionResult> CriarEvento()
        {
            return View();
        }

        /// <summary>
        /// Funçao que permite ao utilizador ver os seus eventos e das pessoas que segue
        /// </summary>
        /// <returns>retorna todos os eventos dele + seguidores</returns>
        [HttpGet]
        public IActionResult InicialEventos()
        {
            var curUser = _userManager.GetUserAsync(User).Result;
            if (curUser == null)
            {
                return NotFound();
            }
            var eventos = _context.eventos.ToList();
            var following = _context.Followers.Where(f => curUser.Id.Equals(f.UserIdThatFollows)).ToList();
            var userEventos = _context.UserEventos.ToList();
            var eventFeed = new HashSet<EventoModel>();
            var curUserInEvents = new List<EventoModel>();
            foreach (var evento in eventos)
            {
                foreach (var userEvento in userEventos)
                {

                    if (userEvento.EventoId == evento.Id && userEvento.UserId.Equals(curUser.Id))
                    {
                        curUserInEvents.Add(evento);
                    }
                }
                continue;
            }
            eventFeed.UnionWith(curUserInEvents);
            foreach (var evento in eventos)
            {
                foreach (var userEvento in userEventos)
                {
                    foreach (var follow in following)
                    {
                        if (userEvento.EventoId == evento.Id && userEvento.UserId.Equals(follow.UserIdFollowed))
                        {
                            eventFeed.Add(evento);
                        }
                    }
                    continue;
                }
            }
            foreach (var evt in eventFeed)
            {
                if (!evt.isActive)
                {
                    eventFeed.Remove(evt);
                }
            }
            ViewData["curUserId"] = curUser.Id;
            return View(eventFeed);
        }

        /// <summary>
        /// Funcao que mostra os eventos do user
        /// </summary>
        /// <returns>retorna todos os seus eventos</returns>
        public IActionResult HistConqEvento()
        {
            var eventos = _context.eventos.ToList();
            return View(eventos);
        }

        /// <summary>
        /// Funcao que permite ver todos os comentarios de um evento
        /// </summary>
        /// <param name="eventoId">Id do evento selecionado</param>
        /// <returns>retorna uma lista de comentarios desse evento</returns>
        public async Task<IActionResult> ComentarioEvento(int eventoId)
        {
            var comentarios = new List<Comentario>();
            foreach (var comment in _context.Comentarios)
            {
                if (comment.EventoId == eventoId)
                {
                    comentarios.Add(comment);
                }
            }
            ViewData["eventoId"] = eventoId;
            return View(comentarios);
        }

        /// <summary>
        /// Função que permite ver fotos de um evento
        /// </summary>
        /// <param name="eventoId">Id de um evento</param>
        /// <returns>retorna uma lista de fotos do evento</returns>
        public IActionResult FotoEvento(int eventoId)
        {
            var fotoList = new List<FotoModel>();
            foreach (var foto in _context.Fotos.ToList())
            {
                if (foto.EventoId == eventoId)
                {
                    fotoList.Add(foto);
                }
            }
            ViewData["eventoId"] = eventoId;
            return View(fotoList);

        }
        /// <summary>
        /// Funcao para criar um evento
        /// </summary>
        /// <param name="evento">Um objeto do tipo evento</param>
        /// <param name="file">um ficheiro para colocar imagem</param>
        /// <returns>retorna uma view para a pagina de eventos com o evento criado</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CriarEvento([Bind("Id, UserLimit, IsPrivate, Description, Name, Location, Date, Country, Sport, IsRecurring, HostUser, UserEventos")] EventoModel evento, IFormFile file, string curUserId)
        {
            try
            {
                var user = _context.Users.FirstOrDefault(u => u.Id.Equals(curUserId));
                var userEvento = new UserEvento
                {
                    User = user,
                    UserId = user.Id,
                    IsHost = true,
                    EventoId = evento.Id,
                    EventoModel = evento,
                };
                evento.HostUser = user;
                evento.UserEventos = new List<UserEvento>();
                evento.UserEventos.Add(userEvento);
                user.UserEventos = new List<UserEvento>();
                user.UserEventos.Add(userEvento);
                // Para o test comentar esta Linha
                _unitOfWork.UploadImage(file);
                if (file != null)
                {
                    evento.ImagePath = file.FileName;
                }
                else
                {
                    evento.ImagePath = "";
                }
                _context.Add(evento);
                await _userManager.UpdateAsync(user);
                await _context.SaveChangesAsync();

                var userEventos = _context.UserEventos.ToList();
                var counter = 0;
                var userConquistas = new List<Conquista>();
                var conquistaFlag = false;
                foreach (var evt in _context.eventos)
                {
                    foreach (var userevento in userEventos)
                    {
                        if (userevento.EventoId == evt.Id)
                        {
                            if (userevento.IsHost && user.Id.Equals(userevento.UserId))
                            {
                                counter++;
                            }
                        }
                    }
                }
                if (counter == 5)
                {
                    var novaCOnquista = new ConquistaUser
                    {
                        UserId = user.Id,
                        ConquistId = 1,
                        DateAchieved = DateTime.Now,
                    };
                    _context.ConquistasUser.Add(novaCOnquista);
                }
                await _context.SaveChangesAsync();
                TempData["SuccessoMensagem"] = "Evento criado com sucesso";
                return RedirectToAction(nameof(VerEvento));
            }
            catch
            {
                TempData["ErroMensagem"] = "Erro ao criar evento";
                return RedirectToAction(nameof(VerEvento));
            }
        }
        public async Task<IActionResult> EditarEvento(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var evento = await _context.eventos.FindAsync(id);
            if (evento != null)
            {
                return View(evento);
            }
            return NotFound();
        }

        /// <summary>
        /// Funcao para editar dados de um evento
        /// </summary>
        /// <param name="id">id do evento</param>
        /// <param name="evento">objeto do tipo evento</param>
        /// <param name="file">file para a imagem</param>
        /// <returns>retorna a pagina de ver os eventos com o evento editado</returns>
        [HttpPost]
        public async Task<IActionResult> EditarEvento(int? id, [Bind("Name, Description, Location, Date, Country, Sport, IsPrivate, IsRecurring, UserLimit")] EventoModel evento, IFormFile file)
        {
            var myEvento = await _context.eventos.FindAsync(id);
            if (myEvento == null) return NotFound();
            if (myEvento.Name != evento.Name)
            {
                myEvento.Name = evento.Name;
            }
            if (myEvento.Description != evento.Description)
            {
                myEvento.Description = evento.Description;
            }
            if (myEvento.Location != evento.Location)
            {
                myEvento.Location = evento.Location;
            }
            if (myEvento.Date != evento.Date)
            {
                myEvento.Date = evento.Date;
            }
            if (myEvento.Country != evento.Country)
            {
                myEvento.Country = evento.Country;
            }
            if (myEvento.Sport != evento.Sport)
            {
                myEvento.Sport = evento.Sport;
            }
            if (myEvento.IsPrivate != evento.IsPrivate)
            {
                myEvento.IsPrivate = evento.IsPrivate;
            }
            if (myEvento.IsRecurring != evento.IsRecurring)
            {
                myEvento.IsRecurring = evento.IsRecurring;
            }
            if (myEvento.UserLimit != evento.UserLimit)
            {
                myEvento.UserLimit = evento.UserLimit;
            }

            //COMENTAR UNITOFWORK E O IF PARA PASSAR NO TESTE
            _unitOfWork.UploadImage(file);
            if (file != null)
            {
                myEvento.ImagePath = file.FileName;
            }
            try
            {
                _context.Update(myEvento);
                await _context.SaveChangesAsync();
                TempData["SuccessoMensagem"] = "Evento editado com sucesso";
                return RedirectToAction(nameof(VerEvento));
            }
            catch (Exception ex)
            {
                TempData["ErroMensagem"] = "Não foi possivel editar evento";
                throw;
            }
        }

        /// <summary>
        /// Funcao que mostra todos os eventos ativos
        /// </summary>
        /// <returns>retorna uma lista de eventos</returns>
        public IActionResult VerEvento()
        {
            List<EventoModel> eventos = _context.eventos.ToList();
            return View(eventos);
        }

        /// <summary>
        /// Funcao que permite ver detalhes de um evento
        /// </summary>
        /// <param name="id">id do evento</param>
        /// <returns>retorna uma view com os dados do evento</returns>
        public async Task<IActionResult> DetalheEvento(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //Para o Teste "VerEventoTest" passar => comentar este IF
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            var evento = _context.eventos.FirstOrDefault(e => e.Id == id);
            if (evento == null)
            {
                return NotFound();
            }
            var userEvento = _context.UserEventos.FirstOrDefault(ue => ue.EventoId == id && ue.IsHost == true);
            //ViewData["user"] = _context.Users.FirstOrDefault(u => u.Id.Equals(userEvento.UserId)).Name;
            ViewData["hostId"] = _context.Users.FirstOrDefault(u => u.Id.Equals(userEvento.UserId)).Id;
            var allUserEvents = _context.UserEventos;
            foreach (var userevent in allUserEvents.ToList())
            {
                if (userevent.EventoId == id && userevent.IsHost == false)
                {
                    var user = _userManager.FindByIdAsync(userevent.UserId).Result;
                    userevent.User = user;
                    evento.UserEventos.Add(userevent);
                }
            }
            return View(evento);
        }

        /// <summary>
        /// Função para ver os detalhes de um evento terminado/destivado
        /// </summary>
        /// <param name="id">id do evento</param>
        /// <returns>retorna uma view com os dados do evento</returns>
        public async Task<IActionResult> DetalheEventoDisable(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //Para o Teste "VerEventoTest" passar => comentar este IF
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Account/Login", new { area = "Identity" });
            }

            var evento = _context.eventos.FirstOrDefault(e => e.Id == id);
            if (evento == null)
            {
                return NotFound();
            }
            var userEvento = _context.UserEventos.FirstOrDefault(ue => ue.EventoId == id && ue.IsHost == true);
            //ViewData["user"] = _context.Users.FirstOrDefault(u => u.Id.Equals(userEvento.UserId)).Name;
            ViewData["hostId"] = _context.Users.FirstOrDefault(u => u.Id.Equals(userEvento.UserId)).Id;
            var allUserEvents = _context.UserEventos;
            foreach (var userevent in allUserEvents.ToList())
            {
                if (userevent.EventoId == id && userevent.IsHost == false)
                {
                    var user = _userManager.FindByIdAsync(userevent.UserId).Result;
                    userevent.User = user;
                    evento.UserEventos.Add(userevent);
                }
            }
            return View(evento);
        }
        /// <summary>
        /// Funcao para apagar um evento
        /// </summary>
        /// <param name="id">id do evento</param>
        /// <returns>retorna uma view da pagina dos eventos existentes</returns>
        public async Task<IActionResult> DeleteEvento(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var evento = _context.eventos.FirstOrDefault(e => e.Id == id);
                _context.eventos.Remove(evento);
                await _context.SaveChangesAsync();
                TempData["SucessoEventoApagado"] = "Evento editado com sucesso";
                return RedirectToAction(nameof(VerEvento));
            }
            catch
            {
                TempData["ErroEventoApagado"] = "Não foi possivel editar evento";
                return RedirectToAction(nameof(VerEvento));
            }
        }
        public async Task<IActionResult> DeleteEventoInicial(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var evento = _context.eventos.FirstOrDefault(e => e.Id == id);
            _context.eventos.Remove(evento);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(InicialEventos));
        }

        /// <summary>
        /// Função para apagar um evento do historico
        /// </summary>
        /// <param name="id">id do evento</param>
        /// <returns>retorna uma view do historico de eventos</returns>
        public async Task<IActionResult> DeleteEventoHistorico(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var evento = _context.eventos.FirstOrDefault(e => e.Id == id);
                _context.eventos.Remove(evento);
                await _context.SaveChangesAsync();
                TempData["SucessoEventoApagado"] = "Evento editado com sucesso";
                return RedirectToAction(nameof(HistConqEvento));
            }
            catch
            {
                TempData["ErroEventoApagado"] = "Não foi possivel editar evento";
                return RedirectToAction(nameof(HistConqEvento));
            }

        }
        public async Task<IActionResult> DisableEvento(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var evento = _context.eventos.FirstOrDefault(e => e.Id == id);
                evento.isActive = false;
                _context.eventos.Update(evento);
                await _context.SaveChangesAsync();
                TempData["SucessoEventoTerminado"] = "Evento terminado com sucesso";
                return RedirectToAction(nameof(InicialEventos));
            }
            catch
            {
                TempData["ErroEventoTerminado"] = "Não foi possivel terminar o evento";
                return RedirectToAction(nameof(InicialEventos));
            }
        }
        [HttpGet]
        public async Task<IActionResult> AddUsers(int? id)
        {
            List<User> users = _userManager.Users.ToList();
            var userEvents = _context.UserEventos;
            var curUserId = _userManager.GetUserIdAsync(_userManager.GetUserAsync(User).Result).Result;
            var usersNotInEvent = new HashSet<User>();
            var excludedUsers = new HashSet<User>();
            foreach (var ue in userEvents)
            {
                if (ue.EventoId != id)
                {
                    if (ue.UserId == curUserId)
                    {
                        excludedUsers.Add(users.FirstOrDefault(u => u.Id.Equals(ue.UserId)));
                        continue;
                    }
                    else
                    {
                        usersNotInEvent.Add(users.FirstOrDefault(u => u.Id.Equals(ue.UserId)));
                    }
                }
                else
                {
                    excludedUsers.Add(users.FirstOrDefault(u => u.Id.Equals(ue.UserId)));
                }
            }
            foreach (var user in users.ToList())
            {
                if (excludedUsers.Contains(user))
                {
                    if (usersNotInEvent.Contains(user))
                    {
                        usersNotInEvent.Remove(user);
                    }
                    continue;
                }
                usersNotInEvent.Add(user);
            }
            foreach (var user in usersNotInEvent)
            {
                if (user.isAdmin)
                {
                    usersNotInEvent.Remove(user); 
                }
            }
            ViewData["Evento"] = id;
            return View(usersNotInEvent);
        }

        /// <summary>
        /// Função que adiociona utilizadores a um evento
        /// </summary>
        /// <param name="userId">Id de um user</param>
        /// <param name="eventId">Id de um evento</param>
        /// <returns>Retorna uma view da pagina de utilizadores atualizada</returns>
        [HttpPost]
        public async Task<IActionResult> AddUsers(string? userId, int eventId)
        {
            try
            {
                var user = _context.Users.FirstOrDefault(u => u.Id.Equals(userId));
                var evt = _context.eventos.FirstOrDefault(e => e.Id == eventId);

                if (evt == null)
                {
                    return NotFound();
                }

                var userEventos = _context.UserEventos.Where(ue => ue.EventoId == eventId).ToList();

                if (userEventos.Count >= evt.UserLimit)
                {
                    TempData["EventoCheio"] = "EventoCheio";
                    return RedirectToAction(nameof(AddUsers));
                }

                var userEvento = new UserEvento
                {
                    User = user,
                    UserId = userId,
                    IsHost = false,
                    EventoId = evt.Id,
                    EventoModel = evt
                };

                _context.Add(userEvento);
                await _userManager.UpdateAsync(user);
                await _context.SaveChangesAsync();
                TempData["SuccessoAdicionado"] = "Membro adicionado com sucesso";
                return RedirectToAction(nameof(AddUsers));
            }
            catch
            {
                TempData["ErroAdicionado"] = "Não foi possivel adicionar membro";
                return RedirectToAction(nameof(AddUsers));
            }

        }

        /// <summary>
        /// Função para mostrar os users de um evento que podem ser removidos
        /// </summary>
        /// <param name="id">id do evento</param>
        /// <returns>Retorna uma view para a página com os utilizadores</returns>
        [HttpGet]
        public async Task<IActionResult> RemoveUsers(int? id)
        {
            List<User> users = _userManager.Users.ToList();
            var userEvents = _context.UserEventos;
            var curUserId = _userManager.GetUserIdAsync(_userManager.GetUserAsync(User).Result).Result;
            var usersInEvent = new HashSet<User>();
            var excludedUsers = new HashSet<User>();
            foreach (var ue in userEvents)
            {
                if (ue.EventoId == id)
                {
                    if (ue.UserId == curUserId && ue.IsHost)
                    {
                        excludedUsers.Add(users.FirstOrDefault(u => u.Id.Equals(ue.UserId)));
                        continue;
                    }
                    else
                    {
                        usersInEvent.Add(users.FirstOrDefault(u => u.Id.Equals(ue.UserId)));
                    }
                }
                else
                {
                    excludedUsers.Add(users.FirstOrDefault(u => u.Id.Equals(ue.UserId)));
                }
            }
            ViewData["Evento"] = id;
            return View(usersInEvent);
        }

        /// <summary>
        /// Função para remover users de um evento
        /// </summary>
        /// <param name="userId">Id do user</param>
        /// <param name="eventId">Id do evento</param>
        /// <returns>Retorna uma view da pagina de utilizadores atualizada</returns>
        [HttpPost]
        public async Task<IActionResult> RemoveUsers(string? userId, int eventId)
        {
            try
            {
                var user = _context.Users.FirstOrDefault(u => u.Id.Equals(userId));
                var evt = _context.eventos.FirstOrDefault(e => e.Id == eventId);

                var userEventos = _context.UserEventos.Where(ue => ue.EventoId == eventId).ToList();

                if (evt == null)
                {
                    return NotFound();
                }

                foreach (var userevento in userEventos)
                {
                    if (userevento.UserId.Equals(userId))
                    {
                        _context.Remove(userevento);
                    }
                }

                await _context.SaveChangesAsync();
                TempData["SuccessoRemovido"] = "Membro removido com sucesso";
                return RedirectToAction(nameof(RemoveUsers));
            }
            catch
            {
                TempData["ErroRemovido"] = "Não foi possivel remover membro";
                return RedirectToAction(nameof(RemoveUsers));
            }

        }

        /// <summary>
        /// Função para entrar num evento
        /// </summary>
        /// <param name="eventoId">Id do evento</param>
        /// <param name="userId">Id do user</param>
        /// <returns>Retorna a página de VerEvento atualizada sem o botão de "Juntar"</returns>
        [HttpPost]
        public async Task<IActionResult> EntrarEvento(int eventoId, string userId)
        {
            try
            {
                var evento = _context.eventos.FirstOrDefault(e => e.Id == eventoId);
                var user = _context.Users.FirstOrDefault(e => e.Id == userId);
                List<EventoModel> _eventos = _context.eventos.ToList();
                List<UserEvento> _userEventos = _context.UserEventos.ToList();

                var flag = false;

                if (evento == null)
                {
                    return NotFound();
                }

                if (user == null)
                {
                    return NotFound();
                }

                var countUserEventos = _context.UserEventos.Where(e => e.EventoId == eventoId).ToList();


                if (countUserEventos.Count >= evento.UserLimit)
                {
                    TempData["ErroEventoCheio"] = "Evento cheio!!! Entre noutro evento";
                    return RedirectToAction(nameof(VerEvento));
                }

                var entrarEvento = new UserEvento();
                entrarEvento.UserId = user.Id;
                entrarEvento.EventoId = evento.Id;
                entrarEvento.User = user;
                entrarEvento.EventoModel = evento;
                entrarEvento.IsHost = false;

                _context.UserEventos.Add(entrarEvento);
                await _context.SaveChangesAsync();

                foreach (var ue in _userEventos)
                {
                    if (ue.UserId == user.Id)
                    {
                        if (evento.Date == ue.EventoModel.Date)
                        {
                            flag = true;
                        }
                    }
                }
                if (flag)
                {
                    await _emailSender.SendEmailAsync(user.Email, "Verifique os seus eventos", $"Já existe um evento com a mesma data que o evento {evento.Name}");
                }
                TempData["SuccessoEntrou"] = "Entrou no evento!!!";
                return RedirectToAction(nameof(VerEvento));
            }
            catch
            {
                TempData["ErroEntrou"] = "Não foi possivel entrar no evento";
                return RedirectToAction(nameof(VerEvento));
            }
        }
        /// <summary>
        /// Função para sair de um evento
        /// </summary>
        /// <param name="eventoId">Id do evento</param>
        /// <param name="userId">Id do user</param>
        /// <returns>Retorna uma View da Página "VerEvento" atualizada sem o user no evento que saiu</returns>
        [HttpPost]
        public async Task<IActionResult> SairEvento(int eventoId, string userId)
        {
            try
            {
                var evento = _context.UserEventos.Where(e => e.EventoId == eventoId).Where(e => e.UserId == userId).FirstOrDefault();

                _context.UserEventos.Remove(evento);
                await _context.SaveChangesAsync();
                TempData["SuccessoSaiu"] = "Saiu do evento!!!";
                return RedirectToAction(nameof(VerEvento));
            }
            catch
            {
                TempData["ErroSaiu"] = "Não foi possivel sair do evento";
                return RedirectToAction(nameof(VerEvento));
            }

        }


        /// <summary>
        /// Função para comentar um evento
        /// </summary>
        /// <param name="eventoId">Id do evento</param>
        /// <param name="commentText">Comentario a adicionar</param>
        /// <returns>Retorna a página de comentários desse evento com o comentario novo</returns>
        [HttpPost]
        public async Task<IActionResult> ComentarEvento(int eventoId, string commentText, string curUserId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id.Equals(curUserId));
            var evento = _context.eventos.FirstOrDefault(c => c.Id == eventoId);
            var comentario = new Comentario
            {
                Comment = commentText,
                EventoId = eventoId,
                UserId = user.Id
            };
            _context.Comentarios.Add(comentario);
            _context.SaveChanges();
            var listaComentarios = new List<Comentario>();
            foreach (var comment in _context.Comentarios.ToList())
            {
                if (comment.EventoId == eventoId)
                {
                    listaComentarios.Add(comment);
                }
            }
            var userEventos = _context.UserEventos.ToList();
            var counter = 0;
            var conqAchivedFlag = false;
            foreach (var cmt in _context.Comentarios.ToList())
            {
                if (comentario.UserId.Equals(user.Id))
                {
                    counter++;
                }
            }
            if (counter == 5)
            {
                foreach (var conq in _context.ConquistasUser.ToList())
                {
                    if (conq.UserId.Equals(user.Id))
                    {
                        if (conq.ConquistId == 4)
                        {
                            conqAchivedFlag = true;
                        }
                    }
                }
                if (!conqAchivedFlag)
                {
                    var novaCOnquista = new ConquistaUser
                    {
                        UserId = user.Id,
                        ConquistId = 4,
                        DateAchieved = DateTime.Now,
                    };
                    _context.ConquistasUser.Add(novaCOnquista);
                }
            }
            await _context.SaveChangesAsync();
            ViewData["eventoId"] = eventoId;
            return RedirectToAction(nameof(ComentarioEvento), new { eventoId = evento.Id });
        }

        /// <summary>
        /// Função para apagar um evento
        /// </summary>
        /// <param name="comentarioId">Id do comentário</param>
        /// <param name="eventoId">Id do evento</param>
        /// <returns>Retorna da página dos comentários do evento sem o comentário apagado</returns>
        [HttpPost]
        public async Task<IActionResult> ApagarComentario(int comentarioId, int eventoId, string curUserId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id.Equals(curUserId));
            var evento = _context.eventos.FirstOrDefault(c => c.Id == eventoId);
            var comentario = _context.Comentarios.FirstOrDefault(c => c.Id == comentarioId);
            _context.Comentarios.Remove(comentario);
            _context.SaveChanges();
            var listaComentarios = new List<Comentario>();
            foreach (var comment in _context.Comentarios.ToList())
            {
                if (comment.EventoId == eventoId)
                {
                    listaComentarios.Add(comment);
                }
            }
            ViewData["eventoId"] = eventoId;
            return RedirectToAction(nameof(ComentarioEvento), new { eventoId = evento.Id });
        }

        /// <summary>
        /// Função para adicionar foto a um evento
        /// </summary>
        /// <param name="eventoId">Id do evento</param>
        /// <param name="file">File para a foto</param>
        /// <returns>Retorna View para a página de detalhes do evento</returns>
        [HttpPost]
        public async Task<IActionResult> AdicionarFoto(int eventoId, IFormFile file, string curUserId)
        {
            var newFoto = new FotoModel
            {
                EventoId = eventoId,
                User = _context.Users.FirstOrDefault(u => u.Id.Equals(curUserId)).Id
            };
            // Para Passar no teste Comentar esta LINHA!
            _unitOfWork.UploadImage(file);
            newFoto.ImagePath = file != null ? file.FileName : "";
            _context.Fotos.Add(newFoto);
            _context.SaveChanges();
            var fotoList = new List<FotoModel>();
            foreach (var foto in _context.Fotos.ToList())
            {
                if (foto.EventoId == eventoId)
                {
                    fotoList.Add(foto);
                }
            }
            var userEventos = _context.UserEventos.ToList();
            var user = _context.Users.FirstOrDefault(u => u.Id.Equals(curUserId));
            var counter = 0;
            var conqAchivedFlag = false;
            foreach (var ft in _context.Fotos.ToList())
            {
                if (newFoto.User.Equals(user))
                {
                    counter++;
                }
            }
            if (counter == 5)
            {
                foreach (var conq in _context.ConquistasUser.ToList())
                {
                    if (conq.UserId.Equals(user))
                    {
                        if (conq.ConquistId == 5)
                        {
                            conqAchivedFlag = true;
                        }
                    }
                }
                if (!conqAchivedFlag)
                {
                    var novaCOnquista = new ConquistaUser
                    {
                        UserId = user.Id,
                        ConquistId = 5,
                        DateAchieved = DateTime.Now,
                    };
                    _context.ConquistasUser.Add(novaCOnquista);
                }
            }
            await _context.SaveChangesAsync();
            ViewData["eventoId"] = eventoId;
            return RedirectToAction(nameof(DetalheEvento), new { id = eventoId });
        }

        /// <summary>
        /// Função para apagar uma foto de um evento
        /// </summary>
        /// <param name="fotoId">Id da foto</param>
        /// <param name="eventoId">Id do evento</param>
        /// <returns>Retorna uma View da página de fotos do evento com a lista de fotos atualizada</returns>
        [HttpPost]
        public async Task<IActionResult> ApagarFoto(int fotoId, int eventoId)
        {
            var fotoRemoved = _context.Fotos.FirstOrDefault(c => c.Id == fotoId);
            var evento = _context.eventos.FirstOrDefault(c => c.Id == eventoId);
            _context.Fotos.Remove(fotoRemoved);
            _context.SaveChanges();
            var fotoList = new List<FotoModel>();
            foreach (var foto in _context.Fotos.ToList())
            {
                fotoList.Add(foto);
            }
            ViewData["eventoId"] = eventoId;
            return RedirectToAction(nameof(FotoEvento), new { eventoId = evento.Id });
        }
        public async Task<IActionResult> Classificar(int eventoId, int rate, string curUserId)
        {
            var evento = _context.eventos.FirstOrDefault(c => c.Id == eventoId);
            var classificacao = new Classificacao
            {
                EventoId = eventoId,
                UserId = _context.Users.FirstOrDefault(u => u.Id.Equals(curUserId)).Id,
                Rate = rate,
            };
            _context.Classificacoes.Add(classificacao);
            _context.SaveChanges();
            var userEvento = _context.UserEventos.FirstOrDefault(ue => ue.EventoId == eventoId && ue.IsHost == true);
            ViewData["hostId"] = _context.Users.FirstOrDefault(u => u.Id.Equals(userEvento.UserId)).Id;
            return RedirectToAction(nameof(DetalheEvento), new { id = evento.Id });
        }
    }
}
