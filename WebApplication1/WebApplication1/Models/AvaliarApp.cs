﻿using WebApplication1.Areas.Identity.Data;

namespace WebApplication1.Models
{
    public class AvaliarApp
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string? Comentario { get; set; }
        public int Rate { get; set; }

    }
}
