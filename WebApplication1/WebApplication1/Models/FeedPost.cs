﻿using WebApplication1.Areas.Identity.Data;

namespace WebApplication1.Models
{
    public class FeedPost
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Post { get; set; }
    }
}
