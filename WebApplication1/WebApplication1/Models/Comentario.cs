﻿using WebApplication1.Areas.Identity.Data;

namespace WebApplication1.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Comment { get; set; }
        public int EventoId { get; set; }
    }
}
