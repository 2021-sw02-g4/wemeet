﻿using WebApplication1.Areas.Identity.Data;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class EventoModel
    {
        [Key]
        public int Id { get; set; }
        public User HostUser { get; set; }
        public int UserLimit { get; set; }
        public bool IsPrivate { get; set; }
        public string? ImagePath { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public DateTime Date { get; set; }
        public string Country { get; set; }
        public string Sport { get; set; }
        public bool IsRecurring { get; set; }
        public bool isActive { get; set; } = true;
        public DateTime CreatedEventoDate { get; set; } = DateTime.Now;
        public ICollection<UserEvento> UserEventos { get; set; }
    }
}