﻿namespace WebApplication1.Models
{
    public class Classificacao
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int EventoId { get; set; }
        public int Rate { get; set; }
    }
}
