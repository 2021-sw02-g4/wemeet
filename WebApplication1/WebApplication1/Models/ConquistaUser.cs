﻿namespace WebApplication1.Models
{
    public class ConquistaUser
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int ConquistId { get; set; }
        public DateTime DateAchieved { get; set; }
    }
}