﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class SocialModel
    {
        [Key]
        public int Id { get; set; } 
        public string UserIdFollowed { get; set; }
        public string UserIdThatFollows { get; set; }
    }
}
