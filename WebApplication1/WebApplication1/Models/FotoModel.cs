﻿using WebApplication1.Areas.Identity.Data;

namespace WebApplication1.Models
{
    public class FotoModel
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
        public int EventoId { get; set; }
        public string User { get; set; }
    }
}
