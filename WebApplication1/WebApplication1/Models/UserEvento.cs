﻿using WebApplication1.Areas.Identity.Data;
using WebApplication1.Models;

namespace WebApplication1.Models
{
    public class UserEvento
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public int EventoId { get; set; }
        public EventoModel EventoModel { get; set; }
        public bool IsHost { get; set; }
    }
}
