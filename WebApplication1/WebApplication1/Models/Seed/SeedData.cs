﻿using Microsoft.AspNetCore.Identity;
using WebApplication1.Areas.Identity.Data;

namespace WebApplication1.Models
{
    public static class SeedData
    {
        public static async Task SeedUserManager(UserManager<User> userManager)
        {
            await SeedUsersAsync(userManager);
        }
        private static async Task SeedUsersAsync(UserManager<User> userManager)
        {
            if (userManager.FindByNameAsync("admin").Result == null)
            {
                var admin = new User
                {
                    PhoneNumber = "999999999",
                    Name = "Admin",
                    BirthDate = DateTime.Now,
                    Hobbies = "Futebol",
                    Country = "Portugal",
                    Address = "Morada Admin",
                    isPrivate = true,
                    isActive = true,
                    isAdmin = true,
                    CreatedAccountDate = DateTime.Now,
                    Email = "admin@gmail.com",
                    UserName = "admin@gmail.com",
                    EmailConfirmed = true
                };
                await userManager.CreateAsync(admin, "Admin123!");
                
            }
        }
    }
}
