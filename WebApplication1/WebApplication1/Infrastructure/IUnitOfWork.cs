﻿namespace WebApplication1.Infrastructure
{
    public interface IUnitOfWork
    {
        void UploadImage(IFormFile file); 
    }
}
