﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;


namespace WebApplication1.Services
{
    public class EmailSender : IEmailSender
    {
         public EmailSenderOptions Options { get; }

        public EmailSender(/*IOptions<EmailSenderOptions> optionsAccess*/)
        {
            //Options = optionsAccess.Value;

        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            return Execute("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", subject, htmlMessage, email);
        }

        public Task Execute(string apiKey, string subject, string message, string email)
        {
            var client = new SendGridClient("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");
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("wemeetesapv2022@gmail.com", "Confirm your Account"),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));

            // Disable click tracking.
            // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
            msg.SetClickTracking(false, false);

            return client.SendEmailAsync(msg);
        }
    }
}
