// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
#nullable disable

using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Infrastructure;

namespace WebApplication1.Areas.Identity.Pages.Account
{
    public class EditProfileModel : PageModel
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        protected IEmailSender _emailSender;

        public EditProfileModel(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IUnitOfWork unitOfWork,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _unitOfWork = unitOfWork;
            _emailSender = emailSender;
        }

        public string ImagePath { get; set; }
        public string Email { get; set; }
        public string Hobbies { get; set; }
        public string Country { get; set; }
        public bool isPrivate { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        [TempData]
        public string StatusMessage { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        [BindProperty]
        public InputModel Input { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public class InputModel
        {
            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Required(ErrorMessage = "* � preciso preencher o campo Email *")]
            [EmailAddress]
            public string NewEmail { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Current password")]
            public string OldPassword { get; set; }

            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New password")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm new password")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required(ErrorMessage = "* � preciso preencher o campo Telem�vel *")]           
            [RegularExpression("([0-9]+)", ErrorMessage = "* O campo Telem�vel, s� pode conter n�meros *")]
            [StringLength(9, MinimumLength = 9, ErrorMessage = "* O campo Telem�vel tem que ter no m�nimo 9 n�meros *")]       
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
            public string Hobbies { get; set; }

            [Required(ErrorMessage = "* � preciso preencher o campo Nome *")]
            public string Name { get; set; }

            public bool isPrivate { get; set; }
            public string Country { get; set; }

            [Required(ErrorMessage = "* � preciso preencher o campo Morada *")]
            public string Address { get; set; }
        }

        private async Task LoadAsync(User user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var email = await _userManager.GetEmailAsync(user);

            Username = userName;
            Email = email;
            ImagePath = user.ImagePath;


            Input = new InputModel
            {
                NewEmail = email,
                PhoneNumber = user.PhoneNumber,
                Country = user.Country,
                Address = user.Address,
                Hobbies = user.Hobbies,
                isPrivate = user.isPrivate,
                Name = user.Name
            };
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
            var user = await _userManager.GetUserAsync(User);

            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {

                await LoadAsync(user);
                return Page();
            }

            var email = await _userManager.GetEmailAsync(user);
            if (Input.NewEmail != email)
            {
                user.Email = Input.NewEmail;
                user.UserName = Input.NewEmail;
            }

            if (Input.NewPassword != null)
            {
                var changePasswordResult = await _userManager.ChangePasswordAsync(user, Input.OldPassword, Input.NewPassword);
            }
            if (Input.PhoneNumber != user.PhoneNumber)
            {
                user.PhoneNumber = Input.PhoneNumber;
            }
            if (Input.Address != user.Address)
            {
                user.Address = Input.Address;
            }
            if (Input.Country != user.Country)
            {
                user.Country = Input.Country;
            }
            if (Input.Hobbies != user.Hobbies)
            {
                user.Hobbies = Input.Hobbies;
            }
            if (Input.isPrivate != user.isPrivate)
            {
                user.isPrivate = Input.isPrivate;
            }

            if (Input.Name != user.Name)
            {
                user.Name = Input.Name;
            }

            _unitOfWork.UploadImage(file);
            if (file != null)
            {
                user.ImagePath = file.FileName;
            }

            await _userManager.UpdateAsync(user);

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Your profile has been updated";
            TempData["SuccessoEditar"] = "Perfil editado com sucesso";
            await _emailSender.SendEmailAsync(user.Email, "Dados alterados", $"Os dados da sua conta foram alterados com sucesso. Para mais informa��o verifique o seu perfil");
            return RedirectToPage();
        }
    }
}
