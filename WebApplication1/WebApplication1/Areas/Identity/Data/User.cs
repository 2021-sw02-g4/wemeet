﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebApplication1.Models;
namespace WebApplication1.Areas.Identity.Data;
// Add profile data for application users by adding properties to the User class
public class User : IdentityUser
{
    [PersonalData]
    public string PhoneNumber { get; set; }
    public string Name { get; set; }
    public DateTime BirthDate { get; set; }
    public string Hobbies { get; set; }
    public string Country { get; set; }
    public string Address { get; set; }
    public bool isPrivate { get; set; }
    public int Age { get; set; }
    public bool isActive { get; set; }
    public string? ImagePath { get; set; }
    public bool isAdmin { get; set; }
    public DateTime CreatedAccountDate { get; set; } = DateTime.Now;
    public ICollection<UserEvento>? UserEventos { get; set; }
    public ICollection<User>? Followers { get; set; } = new HashSet<User>();
}