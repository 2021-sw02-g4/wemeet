﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Areas.Identity.Data;
using WebApplication1.Models;

namespace WebApplication1.Data;
public class WebApplication1Context : IdentityDbContext<User>
{
    public WebApplication1Context(DbContextOptions<WebApplication1Context> options)
        : base(options)
    {
    }    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.Entity<UserEvento>()
            .HasKey(bc => new {bc.UserId, bc.EventoId});

        builder.Entity<UserEvento>()
            .HasOne(bc => bc.User)
            .WithMany(b => b.UserEventos)
            .HasForeignKey(bc => bc.UserId);

        builder.Entity<UserEvento>()
            .HasOne(bc => bc.EventoModel)
            .WithMany(b => b.UserEventos)
            .HasForeignKey(bc => bc.EventoId);

        builder.Entity<Conquista>().HasData(
            new Conquista
            {
                Id = 1,
                Name = "5 Eventos",
                Description = "Já Criaste 5 Eventos!!",
                Badge = "/imagens/5 Eventos.png",
            },
            new Conquista
            {
                Id = 2,
                Name = "5 Seguidores",
                Description = "Já tiveste 5 Pessoas a te Seguir!!",
                Badge = "/imagens/Seguidores.png",
            },
            new Conquista
            {
                Id = 3,
                Name = "5 A seguir",
                Description = "Já Seguiste 5 Pessoas!!",
                Badge = "/imagens/A seguir.png",
            },
            new Conquista
            {
                Id = 4,
                Name = "5 Comentarios",
                Description = "Já Comentaste 5 vezes!!",
                Badge = "/imagens/Comentarios.png",
            },
            new Conquista
            {
                Id = 5,
                Name = "5 Fotografias",
                Description = "Já Inseriste 5 Fotografias!!",
                Badge = "/imagens/Fotografias.png",
            },
            new Conquista
            {
                Id = 6,
                Name = "5 Conquistas",
                Description = "És o maior",
                Badge = "/imagens/Conquistas.png",
            }
            );

        base.OnModelCreating(builder);
    }
    public DbSet<EventoModel> eventos { get; set; }
    public DbSet<UserEvento> UserEventos { get; set; }
    public DbSet<SocialModel> Followers { get; set; }
    public DbSet<AvaliarApp> AvaliarApp { get; set; }
    public DbSet<Comentario> Comentarios { get; set; }
    public DbSet<FotoModel> Fotos { get; set; }
    public DbSet<Classificacao> Classificacoes { get; set; }
    public DbSet<Conquista> Conquistas { get; set; }
    public DbSet<ConquistaUser> ConquistasUser { get; set; }
    public DbSet<FeedPost> FeedPosts { get; set; }
}
